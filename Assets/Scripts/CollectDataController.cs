using UnityEngine;
using System;

public class CollectDataController : MonoBehaviour
{
    public GameObject collectDataGraphModal;
    public event Action<MessageType, string> OnActionSucceed;

    public void SaveData()
    {
        var sensorDataList = DataHolder.currentRawData;
        ServiceLocator.Resolve<IReadWriteController>().SaveData(sensorDataList);
        OnActionSucceed?.Invoke(MessageType.SaveMessage, "Data saved");
    }

    public void CreateMedianFile()
    {
        ServiceLocator.Resolve<IReadWriteController>().GenerateMedianFileFromDataFile();
        OnActionSucceed?.Invoke(MessageType.GenerateMessage, "File generated");
    }

    public void CloseGraphModal()
    {
        var graphManager = FindObjectOfType<GraphManager>();
        graphManager.GetComponent<GraphManager>().ClearPoints();
        ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.None);
        OnActionSucceed?.Invoke(MessageType.None, "");
    }

    public void CloseDataCollectPanel() 
    {
        ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.DabScreen);
        OnActionSucceed?.Invoke(MessageType.None, "");
    }
}
