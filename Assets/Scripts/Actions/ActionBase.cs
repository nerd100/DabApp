using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ActionBase : MonoBehaviour
{
    public void CalculatePoint(List<DataObject> currentData, List<DataObject> targetData)
    {
        var finalPoints = 1000f;
        for (int i = 0; i < currentData.Count; i++)
        {
            var currentValue = currentData[i];
            var targetValue = targetData[i];

            var difference = Mathf.Abs(currentValue.Value - targetValue.Value); //TODO factor entfernen
            finalPoints -= GetPointsToSubstract(difference);
        }

        DataHolder.FinalPoints = finalPoints;
    }

    public bool ValidateDab(List<DataObject> graviYDataList)
    {
        var startPointNormalized = new List<DataObject>();
        var allPointsNormalized = new List<DataObject>();

        var startValue = graviYDataList.Where(data => data.Id == 1).Select(data => data.Value).First();

        if (startValue > 0)
        {
            graviYDataList.ForEach(data => startPointNormalized.Add(new DataObject(data.Id, data.Value - startValue)));
        }
        else
        {
            graviYDataList.ForEach(data => startPointNormalized.Add(new DataObject(data.Id, data.Value + Mathf.Abs(startValue))));
        }

        var highestValue = startPointNormalized.Max(data => Mathf.Abs(data.Value));

        var diff = Mathf.Abs(highestValue - startPointNormalized[0].Value);

        if (diff <= 3)
        {
            return false;
        }

        return true;
    }

    public List<DataObject> NormalizeData(List<DataObject> dataList)
    {
        var startPointNormalized = new List<DataObject>();
        var allPointsNormalized = new List<DataObject>();

        var startValue = dataList.Where(data => data.Id == 1).Select(data => data.Value).First();

        if (startValue > 0)
        {
            dataList.ForEach(data => startPointNormalized.Add(new DataObject(data.Id, data.Value - startValue)));
        }
        else
        {
            dataList.ForEach(data => startPointNormalized.Add(new DataObject(data.Id, data.Value + Mathf.Abs(startValue))));
        }

        var highestValue = startPointNormalized.Max(data => Mathf.Abs(data.Value));

        startPointNormalized.ForEach(data => allPointsNormalized.Add(new DataObject(data.Id, (data.Value / highestValue) * 100)));

        return allPointsNormalized;
    }

    private float GetPointsToSubstract(float diff)
    {
        if (diff <= 1)
        {
            return 0;
        }
        else if (diff < 1 && diff <= 5)
        {
            return 1;
        }
        else if (diff < 5 && diff <= 10)
        {
            return 2;
        }
        else if (diff < 10f && diff <= 15)
        {
            return 3;
        }
        else if (diff < 15f && diff <= 20)
        {
            return 4;
        }
        else if (diff < 20 && diff <= 25)
        {
            return 5;
        }
        else if (diff < 25 && diff <= 30)
        {
            return 6;
        }
        else if (diff < 30 && diff <= 35)
        {
            return 7;
        }
        else if (diff < 35 && diff <= 40)
        {
            return 8;
        }
        else if (diff < 40 && diff <= 45)
        {
            return 9;
        }
        else
        {
            return 10;
        }
    }
}
