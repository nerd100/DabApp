using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MimicAction : ActionBase, IEvaluateAction
{
    public void Evaluate()
    {
        ServiceLocator.Resolve<ISensorService>().Reset();
        var firstMimicData = DataHolder.mimicFirstRawData;
        var secondMimicData = DataHolder.mimicSecondRawData;

        var readWriteController = ServiceLocator.Resolve<IReadWriteController>();

        var firstMimicSensorData = readWriteController.CombineSensorData(firstMimicData);
        var secondMimicSensorData = readWriteController.CombineSensorData(secondMimicData);


        var normalizedFirstMimicSensorData = NormalizeData(firstMimicSensorData);
        var normalizedSecondMimicSensorData = NormalizeData(secondMimicSensorData);

        DataHolder.TargetNormalizedSensorDataList = normalizedFirstMimicSensorData.ToList();
        DataHolder.CurrentNormalizedSensorDataList = normalizedSecondMimicSensorData.ToList();

        CalculatePoint(normalizedSecondMimicSensorData, normalizedFirstMimicSensorData);

        ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.None);
        ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.ResultScreen);

        GameManager.instance.StartEvaluation();
    }
}
