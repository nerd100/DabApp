using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class DabAction : ActionBase, IEvaluateAction
{
    public void Evaluate()
    {
        ServiceLocator.Resolve<ISensorService>().Reset();
        var sensorDataList = DataHolder.currentRawData;

        var validationResult = ValidateDab(sensorDataList[7].Data); //GraviY

        if (validationResult)
        {
            var readWriteController = ServiceLocator.Resolve<IReadWriteController>();

            var combinedSensorData = readWriteController.CombineSensorData(sensorDataList);
            var targetSensorData = readWriteController.ReadAndConvertMedianFile();

            var normalizedCombinedSensorData = NormalizeData(combinedSensorData);
            var normalizedTargetSensorData = NormalizeData(targetSensorData);

            DataHolder.CurrentNormalizedSensorDataList = normalizedCombinedSensorData.ToList();
            DataHolder.TargetNormalizedSensorDataList = normalizedTargetSensorData.ToList();

            CalculatePoint(normalizedCombinedSensorData, normalizedTargetSensorData);

            ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.None);
            ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.ResultScreen);

            GameManager.instance.StartEvaluation();
        }
        else
        {
            ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.VerdabbtModal);
            FindObjectOfType<AudioManager>().Play("Verdabbt");
        }
    }
}
