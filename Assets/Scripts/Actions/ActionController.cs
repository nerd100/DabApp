using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ActionController : MonoBehaviour
{
    internal void Evaluate()
    {
        GetComponent<IEvaluateAction>().Evaluate();
    }
}
