using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MimicTitle : MonoBehaviour
{
    public GameObject mimicTitleText;
    public GameObject mimicButtonImage;
    public Sprite mimicSprite1;
    public Sprite mimicSprite2;

    private void Awake()
    {
        ServiceLocator.Resolve<IActionHandler>().OnActionChanged += ChangeTitle;
    }

    private void Start()
    {
        mimicTitleText.GetComponent<TMP_Text>().text = "Show your moves";
    }

    private void ChangeTitle(ActionTypeEnum actionState)
    {
        if (actionState == ActionTypeEnum.None || actionState == ActionTypeEnum.MimicFirst)
        {
            mimicTitleText.GetComponent<TMP_Text>().text = "Show your moves";
            mimicButtonImage.GetComponent<Image>().sprite = mimicSprite1;
        }
        else if(actionState == ActionTypeEnum.MimicSecond)
        {
            mimicTitleText.GetComponent<TMP_Text>().text = "Mimic that";
            mimicButtonImage.GetComponent<Image>().sprite = mimicSprite2;
        }
    }
}
