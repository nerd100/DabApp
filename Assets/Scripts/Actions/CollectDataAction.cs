using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectDataAction : ActionBase, IEvaluateAction
{
    public void Evaluate()
    {
        ServiceLocator.Resolve<ISensorService>().Reset();
        var sensorDataList = DataHolder.currentRawData;

        ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.CollectDataGraphModal);

        Transform graphPanel = gameObject.transform.Find("CollectDataGraphModal/GraphPanel");
        var graphManager = graphPanel.GetComponent<GraphManager>();
        graphManager.DrawCombinedSensorGraph(sensorDataList, 0);
    }
}