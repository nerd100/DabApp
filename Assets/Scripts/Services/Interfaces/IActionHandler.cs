using System;

public interface IActionHandler 
{
    ActionTypeEnum ActionTypeEnum { get; set; }
    void ChangeAction(int actionId);
    event Action<ActionTypeEnum> OnActionChanged;
}
