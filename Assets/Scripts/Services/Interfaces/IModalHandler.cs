using System;

public interface IModalHandler
{
    void ChangeModal(int modalId);
}