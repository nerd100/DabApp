public interface ISensorService 
{
    void InitAndActivateSensors();
    void Reset();
}
