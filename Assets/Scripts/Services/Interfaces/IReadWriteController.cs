using System.Collections.Generic;

public interface IReadWriteController
{
    List<DataObject> ReadAndConvertMedianFile();
    List<DataObject> CombineSensorData(List<SensorDataObject> allSensorDataList);

    void GenerateMedianFileFromDataFile();
    void SaveData(List<SensorDataObject> sensorDataList);
}
