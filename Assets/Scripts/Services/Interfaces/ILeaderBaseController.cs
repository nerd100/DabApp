using System.Collections.Generic;

public interface ILeaderBaseController
{
    Dictionary<string, LeaderObject> Top10Leader { get; set; }
    void InitLeader();
    void SetNewLeader(string name, int score);
    bool IsNewHighscore(int score);
}
