
public interface IPanelHandler
{
    GameStateEnum PreviousState { get; set; }
    GameStateEnum CurrentState { get; set; }
    void ChangePanel(int panelId);
}
