using System.Collections.Generic;
using UnityEngine;

public class LeaderBaseController : ILeaderBaseController
{
    public Dictionary<string, LeaderObject> Top10Leader { get; set; } = new Dictionary<string, LeaderObject>();

    public void InitLeader()
    {
        if(Top10Leader.Count == 0)
        {
            for (int i = 1; i <= 10; i++)
            {
                var leaderAndScore = PlayerPrefs.GetString("leader" + i, "");
                if (leaderAndScore != "")
                {
                    var leaderAndScoreArray = leaderAndScore.Split(';');
                    var name = leaderAndScoreArray[0];
                    var score = int.Parse(leaderAndScoreArray[1]);
                    Top10Leader.Add("leader" + i, new LeaderObject(name, score));
                }
                else
                {
                    Top10Leader.Add("leader" + i, new LeaderObject("###", 0));
                }
            }
        }
    }

    public void SetNewLeader(string name, int score)
    {
        LeaderObject newLeader = new LeaderObject(name, score);
        LeaderObject nextLeader = null;
        for(int i = 1; i <= Top10Leader.Count; i++)
        {
            if(nextLeader != null)
            {
                newLeader = nextLeader;
            }

            var currentLeader = Top10Leader["leader" + i];
            if(currentLeader.Score < newLeader.Score)
            {
                if(i != 10)
                {
                    nextLeader = currentLeader;
                }
                else
                {
                    nextLeader = null;
                }

                Top10Leader["leader" + i] = newLeader;
            }
        }

        SaveInPrefs();
    }

    private void SaveInPrefs()
    {
        for (int i = 1; i <= 10; i++)
        {
            var name = Top10Leader["leader" + i].Name;
            var score = Top10Leader["leader" + i].Score;
            PlayerPrefs.SetString("leader" + i, name + ";" + score);
        }
    }

    public bool IsNewHighscore(int score)
    {
        foreach (var leader in Top10Leader.Values)
        {
            if (leader.Score < score)
            {
                return true;
            }
        }

        return false;
    }
}
