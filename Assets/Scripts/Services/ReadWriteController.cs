using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

public class ReadWriteController : IReadWriteController
{
    private const string saveFilePath = "Assets/Resources/sensorData.txt";
    private string saveAndroidFilePath;

    private const string readFilePath = "Assets/Resources/sensorData.txt";
    private static string androidReadFilePath;

    private const string writeMedianFilePath = "Assets/Resources/sensorMedianData.txt";
    private string androidWriteMedianFilePath;

    public ReadWriteController()
    {
        saveAndroidFilePath = Application.persistentDataPath + "sensorData.txt";
        androidReadFilePath = Application.persistentDataPath + "sensorData.txt";
        androidWriteMedianFilePath = Application.persistentDataPath + "sensorMedianData.txt";
    }

    public async void GenerateMedianFileFromDataFile()
    {
        ListSensorDataObject gyroXSensorDataList = new ListSensorDataObject();
        ListSensorDataObject gyroYSensorDataList = new ListSensorDataObject();
        ListSensorDataObject gyroZSensorDataList = new ListSensorDataObject();

        ListSensorDataObject accXSensorDataList = new ListSensorDataObject();
        ListSensorDataObject accYSensorDataList = new ListSensorDataObject();
        ListSensorDataObject accZSensorDataList = new ListSensorDataObject();

        ListSensorDataObject graviXSensorDataList = new ListSensorDataObject();
        ListSensorDataObject graviYSensorDataList = new ListSensorDataObject();
        ListSensorDataObject graviZSensorDataList = new ListSensorDataObject();

        StreamReader reader;

        if (Application.platform == RuntimePlatform.Android)
        {
            reader = new StreamReader(androidReadFilePath);
        }
        else
        {
            reader = new StreamReader(readFilePath);
        }

        string line;

        while ((line = await reader.ReadLineAsync()) != null)
        {
            var sensor = JObject.Parse(line)["Sensor"].ToString();
            List<DataObject> data;
            switch (sensor)
            {
                case nameof(SensorEnum.GyroX):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    gyroXSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("GyroX", data));
                    break;
                case nameof(SensorEnum.GyroY):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    gyroYSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("GyroY", data));
                    break;
                case nameof(SensorEnum.GyroZ):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    gyroZSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("GyroZ", data));
                    break;
                case nameof(SensorEnum.AccX):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    accXSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("AccX", data));
                    break;
                case nameof(SensorEnum.AccY):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    accYSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("AccY", data));
                    break;
                case nameof(SensorEnum.AccZ):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    accZSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("AccZ", data));
                    break;
                case nameof(SensorEnum.GraviX):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    graviXSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("GraviX", data));
                    break;
                case nameof(SensorEnum.GraviY):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    graviYSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("GraviY", data));
                    break;
                case nameof(SensorEnum.GraviZ):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    graviZSensorDataList.SpecificSensorDataList.Add(new SensorDataObject("GraviZ", data));
                    break;
            }
        }

        reader.Close();

        AllSensorData allSensorDataObject = new AllSensorData();

        allSensorDataObject.AllGyroXSensorDataList = gyroXSensorDataList;
        allSensorDataObject.AllGyroYSensorDataList = gyroYSensorDataList;
        allSensorDataObject.AllGyroZSensorDataList = gyroZSensorDataList;

        allSensorDataObject.AllAccXSensorDataList = accXSensorDataList;
        allSensorDataObject.AllAccYSensorDataList = accYSensorDataList;
        allSensorDataObject.AllAccZSensorDataList = accZSensorDataList;

        allSensorDataObject.AllGraviXSensorDataList = graviXSensorDataList;
        allSensorDataObject.AllGraviYSensorDataList = graviYSensorDataList;
        allSensorDataObject.AllGraviZSensorDataList = graviZSensorDataList;

        GenerateMedianFile(allSensorDataObject);
    }

    public List<DataObject> ReadAndConvertMedianFile()
    {
        List<SensorDataObject> resultList = new List<SensorDataObject>();

        StreamReader reader;

        if (Application.platform == RuntimePlatform.Android)
        {
            reader = new StreamReader(androidReadFilePath);
        }
        else
        {
            reader = new StreamReader(readFilePath);
        }

        string line;

        while ((line = reader.ReadLine()) != null)
        {
            var sensor = JObject.Parse(line)["Sensor"].ToString();
            List<DataObject> data;
            switch (sensor)
            {
                case nameof(SensorEnum.GyroX):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("GyroX", data));
                    break;
                case nameof(SensorEnum.GyroY):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("GyroY", data));
                    break;
                case nameof(SensorEnum.GyroZ):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("GyroZ", data));
                    break;
                case nameof(SensorEnum.AccX):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("AccX", data));
                    break;
                case nameof(SensorEnum.AccY):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("AccY", data));
                    break;
                case nameof(SensorEnum.AccZ):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("AccZ", data));
                    break;
                case nameof(SensorEnum.GraviX):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("GraviX", data));
                    break;
                case nameof(SensorEnum.GraviY):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("GraviY", data));
                    break;
                case nameof(SensorEnum.GraviZ):
                    data = JsonConvert.DeserializeObject<List<DataObject>>(JObject.Parse(line)["Data"].ToString());
                    resultList.Add(new SensorDataObject("GraviZ", data));
                    break;
            }
        }

        reader.Close();

        var combinedResultList = CombineSensorData(resultList);
        return combinedResultList;
    }

    private List<DataObject> CreateDataFor(List<SensorDataObject> specificSensorDataList)
    {
        List<DataObject> resultList = new List<DataObject>();

        for (int i = 0; i < 1; i++)
            {
                for (int j = 0; j < specificSensorDataList[i].Data.Count; j++)
                {
                    var value = specificSensorDataList[i].Data[j].Value;
                    for (int k = 1; k < specificSensorDataList.Count; k++)
                    {
                        value += specificSensorDataList[k].Data[j].Value;
                    }

                    var calculatedValue = value / specificSensorDataList.Count;
                    var roundedCalculatedValue = (float)Math.Round(calculatedValue, 3);
                    resultList.Add(new DataObject(j + 1, roundedCalculatedValue));
                }
            }

        return resultList;
    }

    public List<DataObject> CombineSensorData(List<SensorDataObject> allSensorDataList)
    {
        List<DataObject> combinedSensorData = new List<DataObject>();
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < allSensorDataList[i].Data.Count; j++)
            {
                var value = allSensorDataList[i].Data[j].Value;
                for (int k = 1; k < allSensorDataList.Count; k++)
                {
                    value += allSensorDataList[k].Data[j].Value;
                }
                var calculatedValue = value / allSensorDataList.Count;
                combinedSensorData.Add(new DataObject(j + 1, calculatedValue));
            }

        }

        return combinedSensorData;
    }

    public void SaveData(List<SensorDataObject> sensorDataList)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            WriteToFile(saveAndroidFilePath, sensorDataList, true);
        }
        else
        {
            WriteToFile(saveFilePath, sensorDataList, true);
        }
    }

    private void WriteToFile(string filepath, List<SensorDataObject> sensorDataObjects, bool append = false)
    {
        StreamWriter writer = new StreamWriter(filepath, append);

        foreach (var sensorDataObject in sensorDataObjects)
        {
            writer.WriteLine(JsonConvert.SerializeObject(sensorDataObject));
        }

        writer.Close();

    }

    private void GenerateMedianFile(AllSensorData allSensorDataObject)
    {
        Debug.Log("Generate Median File");

        List<DataObject> gyroXResultList = CreateDataFor(allSensorDataObject.AllGyroXSensorDataList.SpecificSensorDataList);
        List<DataObject> gyroYResultList = CreateDataFor(allSensorDataObject.AllGyroYSensorDataList.SpecificSensorDataList);
        List<DataObject> gyroZResultList = CreateDataFor(allSensorDataObject.AllGyroZSensorDataList.SpecificSensorDataList);


        List<DataObject> accXResultList = CreateDataFor(allSensorDataObject.AllAccXSensorDataList.SpecificSensorDataList);
        List<DataObject> accYResultList = CreateDataFor(allSensorDataObject.AllAccYSensorDataList.SpecificSensorDataList);
        List<DataObject> accZResultList = CreateDataFor(allSensorDataObject.AllAccZSensorDataList.SpecificSensorDataList);

        List<DataObject> graviXResultList = CreateDataFor(allSensorDataObject.AllGraviXSensorDataList.SpecificSensorDataList);
        List<DataObject> graviYResultList = CreateDataFor(allSensorDataObject.AllGraviYSensorDataList.SpecificSensorDataList);
        List<DataObject> graviZResultList = CreateDataFor(allSensorDataObject.AllGraviZSensorDataList.SpecificSensorDataList);

        SensorDataObject sensorMedianGyroX = new SensorDataObject("GyroX", gyroXResultList);
        SensorDataObject sensorMedianGyroY = new SensorDataObject("GyroY", gyroYResultList);
        SensorDataObject sensorMedianGyroZ = new SensorDataObject("GyroZ", gyroZResultList);

        SensorDataObject sensorMedianAccX = new SensorDataObject("AccX", accXResultList);
        SensorDataObject sensorMedianAccY = new SensorDataObject("AccY", accYResultList);
        SensorDataObject sensorMedianAccZ = new SensorDataObject("AccZ", accZResultList);

        SensorDataObject sensorMedianGraviX = new SensorDataObject("GraviX", graviXResultList);
        SensorDataObject sensorMedianGraviY = new SensorDataObject("GraviY", graviYResultList);
        SensorDataObject sensorMedianGraviZ = new SensorDataObject("GraviZ", graviZResultList);

        var sensorDataList = new List<SensorDataObject> {
            sensorMedianGyroX,
            sensorMedianGyroY,
            sensorMedianGyroZ,
            sensorMedianAccX,
            sensorMedianAccY,
            sensorMedianAccZ,
            sensorMedianGraviX,
            sensorMedianGraviY,
            sensorMedianGraviZ
        };

        if (Application.platform == RuntimePlatform.Android)
        {
            WriteToFile(androidWriteMedianFilePath, sensorDataList);
        }
        else
        {
            WriteToFile(writeMedianFilePath, sensorDataList);
        }
    }
}
