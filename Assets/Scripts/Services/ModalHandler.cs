using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ModalHandler : MonoBehaviour, IModalHandler
{
    [SerializeField] private ModalEnum modalState;

    public ModalEnum ModalState { get => modalState; set => modalState = value; }

    [SerializeField] private GameObject verdabbtModal;
    [SerializeField] private GameObject countdownModal;
    [SerializeField] private GameObject countdownMimicModal;
    [SerializeField] private GameObject countdownCollectDataModal;
    [SerializeField] private GameObject statistikModal;
    [SerializeField] private GameObject leaderboardModal;
    [SerializeField] private GameObject dataCollectModal;
    [SerializeField] private GameObject newHighscoreModal;

    public void ChangeModal(int modalId)
    {
        ModalState = (ModalEnum)modalId;
        verdabbtModal.SetActive(ModalState == ModalEnum.VerdabbtModal);
        countdownModal.SetActive(ModalState == ModalEnum.CountdownModal);
        countdownMimicModal.SetActive(ModalState == ModalEnum.CountdownMimicModal);
        countdownCollectDataModal.SetActive(ModalState == ModalEnum.CountdownCollectDataModal);
        statistikModal.SetActive(ModalState == ModalEnum.StatisikModal);
        leaderboardModal.SetActive(ModalState == ModalEnum.LeaderboardModal);
        dataCollectModal.SetActive(ModalState == ModalEnum.CollectDataGraphModal);
        newHighscoreModal.SetActive(ModalState == ModalEnum.NewHighscoreModal);
    }
}
