using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ActionHandler : MonoBehaviour, IActionHandler
{
    [SerializeField] private ActionTypeEnum actionTypeEnum;

    public event Action<ActionTypeEnum> OnActionChanged;

    public ActionTypeEnum ActionTypeEnum { get => actionTypeEnum; set => actionTypeEnum = value; }

    public void ChangeAction(int actionId)
    {
        ActionTypeEnum = (ActionTypeEnum)actionId;
        OnActionChanged?.Invoke(ActionTypeEnum);
    }
}
