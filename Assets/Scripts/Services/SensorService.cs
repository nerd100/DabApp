using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;

public class SensorService : MonoBehaviour, ISensorService
{
    private List<DataObject> gyroX;
    private List<DataObject> gyroY;
    private List<DataObject> gyroZ;

    private List<DataObject> accX;
    private List<DataObject> accY;
    private List<DataObject> accZ;

    private List<DataObject> graviX;
    private List<DataObject> graviY;
    private List<DataObject> graviZ;

    private SensorDataObject sensorGyroX;
    private SensorDataObject sensorGyroY;
    private SensorDataObject sensorGyroZ;

    private SensorDataObject sensorAccX;
    private SensorDataObject sensorAccY;
    private SensorDataObject sensorAccZ;

    private SensorDataObject sensorGraviX;
    private SensorDataObject sensorGraviY;
    private SensorDataObject sensorGraviZ;

    private List<SensorDataObject> SensorDataList { get; set; }

    private bool IsDabbing = false;
    public bool DabSuccessful { get; set; } = false;

    private int counter = 1;

    private int roundValue = 1;

    private float falseThreshhold = 0.3f;

    void FixedUpdate()
    {
        if (Application.platform == RuntimePlatform.Android && IsDabbing)
        {
            var gyroXSensor = UnityEngine.InputSystem.Gyroscope.current.angularVelocity.x.ReadValue();
            gyroX.Add(new DataObject(counter, (float)Math.Round(gyroXSensor < falseThreshhold ? 0.0f : gyroXSensor, roundValue)));
            var gyroYSensor = UnityEngine.InputSystem.Gyroscope.current.angularVelocity.y.ReadValue();
            gyroY.Add(new DataObject(counter, (float)Math.Round(gyroYSensor < falseThreshhold ? 0.0f : gyroYSensor, roundValue)));
            var gyroZSensor = UnityEngine.InputSystem.Gyroscope.current.angularVelocity.z.ReadValue();
            gyroZ.Add(new DataObject(counter, (float)Math.Round(gyroZSensor < falseThreshhold ? 0.0f : gyroZSensor, roundValue)));

            var accXSensor = Accelerometer.current.acceleration.x.ReadValue();
            accX.Add(new DataObject(counter, (float)Math.Round(accXSensor < falseThreshhold ? 0.0f : accXSensor, roundValue)));
            var accYSensor = Accelerometer.current.acceleration.y.ReadValue();
            accY.Add(new DataObject(counter, (float)Math.Round(accYSensor < falseThreshhold ? 0.0f : accYSensor, roundValue)));
            var accZSensor = Accelerometer.current.acceleration.z.ReadValue();
            accZ.Add(new DataObject(counter, (float)Math.Round(accZSensor < falseThreshhold ? 0.0f : accZSensor, roundValue)));

            var graviXSensor = GravitySensor.current.gravity.x.ReadValue();
            graviX.Add(new DataObject(counter, (float)Math.Round(graviXSensor < falseThreshhold ? 0.0f : graviXSensor, roundValue)));
            var graviYSensor = GravitySensor.current.gravity.y.ReadValue();
            graviY.Add(new DataObject(counter, (float)Math.Round(graviYSensor < falseThreshhold ? 0.0f : graviYSensor, roundValue)));
            var graviZSensor = GravitySensor.current.gravity.z.ReadValue();
            graviZ.Add(new DataObject(counter, (float)Math.Round(graviZSensor < falseThreshhold ? 0.0f : graviZSensor, roundValue)));

            counter++;
        }
        else if(IsDabbing)
        {
            gyroX.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));
            gyroY.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));
            gyroZ.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));

            accX.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));
            accY.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));
            accZ.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));

            graviX.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));
            graviY.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));
            graviZ.Add(new DataObject(counter, (float)Math.Round(UnityEngine.Random.Range(-10.000f, 10.000f), roundValue)));

            counter++;
        }

        if (counter >= 101 && IsDabbing)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                DisableSensors();
            }

            IsDabbing = false;

            sensorGyroX = new SensorDataObject("GyroX", gyroX);
            sensorGyroY = new SensorDataObject("GyroY", gyroY);
            sensorGyroZ = new SensorDataObject("GyroZ", gyroZ);

            sensorAccX = new SensorDataObject("AccX", accX);
            sensorAccY = new SensorDataObject("AccY", accY);
            sensorAccZ = new SensorDataObject("AccZ", accZ);

            sensorGraviX = new SensorDataObject("GraviX", graviX);
            sensorGraviY = new SensorDataObject("GraviY", graviY);
            sensorGraviZ = new SensorDataObject("GraviZ", graviZ);

            SensorDataList = new List<SensorDataObject> {
                sensorGyroX,
                sensorGyroY,
                sensorGyroZ,
                sensorAccX,
                sensorAccY,
                sensorAccZ,
                sensorGraviX,
                sensorGraviY,
                sensorGraviZ
            };

            var currentAction = ServiceLocator.Resolve<IActionHandler>().ActionTypeEnum;

            if(currentAction == ActionTypeEnum.MimicFirst)
            {
                DataHolder.mimicFirstRawData = SensorDataList;
                ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.MimicScreen);
                ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.None);
                ServiceLocator.Resolve<IActionHandler>().ChangeAction((int)ActionTypeEnum.MimicSecond);
            }
            else if (currentAction == ActionTypeEnum.MimicSecond)
            {
                DataHolder.mimicSecondRawData = SensorDataList;
                FindObjectOfType<ActionController>().Evaluate();
            }
            else if(currentAction == ActionTypeEnum.RealDab)
            {
                DataHolder.currentRawData = SensorDataList;
                FindObjectOfType<ActionController>().Evaluate();
            }
            else
            {
                DataHolder.currentRawData = SensorDataList;
                FindObjectOfType<ActionController>().Evaluate();
            }
        }
    }

    public void InitAndActivateSensors()
    {
        if (Application.platform == RuntimePlatform.Android &&
            UnityEngine.InputSystem.Gyroscope.current != null &&
            Accelerometer.current != null &&
            GravitySensor.current != null)
        {
            var frequency = 10;
            SetSamplingFrequency(frequency);
            EnableSensors();
        }

        InitializeSensorLists();

        counter = 1;
        IsDabbing = true;
    }

    private void InitializeSensorLists()
    {
        gyroX = new List<DataObject>();
        gyroY = new List<DataObject>();
        gyroZ = new List<DataObject>();

        accX = new List<DataObject>();
        accY = new List<DataObject>();
        accZ = new List<DataObject>();

        graviX = new List<DataObject>();
        graviY = new List<DataObject>();
        graviZ = new List<DataObject>();
    }

    private void SetSamplingFrequency(float frequency)
    {

        UnityEngine.InputSystem.Gyroscope.current.samplingFrequency = frequency;
        Accelerometer.current.samplingFrequency = frequency;
        GravitySensor.current.samplingFrequency = frequency;
    }

    private void EnableSensors()
    {
        InputSystem.EnableDevice(UnityEngine.InputSystem.Gyroscope.current);
        InputSystem.EnableDevice(Accelerometer.current);
        InputSystem.EnableDevice(GravitySensor.current);
    }

    private void DisableSensors()
    {
        InputSystem.DisableDevice(UnityEngine.InputSystem.Gyroscope.current);
        InputSystem.DisableDevice(Accelerometer.current);
        InputSystem.DisableDevice(GravitySensor.current);
    }

    public void Reset()
    {
        IsDabbing = false;
        DabSuccessful = false;
        counter = 1;
    }
}
