using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelHandler : MonoBehaviour, IPanelHandler
{
    [SerializeField] private GameStateEnum currentGameState;
    [SerializeField] private GameStateEnum previousState;

    public GameStateEnum PreviousState { get => previousState; set => previousState = value; }
    public GameStateEnum CurrentState { get => currentGameState; set => currentGameState = value; }

    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject dabPanel;
    [SerializeField] private GameObject collectDataPanel;
    [SerializeField] private GameObject mimicPanel;
    [SerializeField] private GameObject resultPanel;

    public void ChangePanel(int panelId)
    {
        PreviousState = CurrentState;
        CurrentState = (GameStateEnum)panelId;
        mainPanel.SetActive(CurrentState == GameStateEnum.StartScreen);
        dabPanel.SetActive(CurrentState == GameStateEnum.DabScreen || CurrentState == GameStateEnum.Dab);
        collectDataPanel.SetActive(CurrentState == GameStateEnum.CollectScreen || CurrentState == GameStateEnum.CollectDataDab);
        resultPanel.SetActive(CurrentState == GameStateEnum.ResultScreen);
        mimicPanel.SetActive(CurrentState == GameStateEnum.MimicScreen || CurrentState == GameStateEnum.Mimic);
    }
}
