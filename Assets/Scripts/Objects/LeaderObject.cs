using System.Collections;
using System.Collections.Generic;

public class LeaderObject
{
    public string Name { get; set; }
    public int Score { get; set; }

    public LeaderObject(string name, int score)
    {
        Name = name;
        Score = score;
    }
}
