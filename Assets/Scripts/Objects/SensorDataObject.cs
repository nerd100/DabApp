using System.Collections;
using System.Collections.Generic;


public class SensorDataObject
{
    public string Sensor { get; set; }

    public List<DataObject> Data { get; set; }

    public SensorDataObject(string Sensor, List<DataObject> Data)
    {
        this.Sensor = Sensor;
        this.Data = Data;
    }
}
