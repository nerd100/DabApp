using System.Collections;
using System.Collections.Generic;

public class DataObject
{
    public int Id { get; set; }

    public float Value { get; set; }

    public DataObject(int Id, float Value)
    {
        this.Id = Id;
        this.Value = Value;
    }
}
