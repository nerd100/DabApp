using System.Collections;
using System.Collections.Generic;

public class ListSensorDataObject
{
    public List<SensorDataObject> SpecificSensorDataList { get; set; } = new List<SensorDataObject>();
}
