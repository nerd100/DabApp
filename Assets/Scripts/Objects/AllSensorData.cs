using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllSensorData
{
    public ListSensorDataObject AllGyroXSensorDataList { get; set; } = new ListSensorDataObject();
    public ListSensorDataObject AllGyroYSensorDataList { get; set; } = new ListSensorDataObject();
    public ListSensorDataObject AllGyroZSensorDataList { get; set; } = new ListSensorDataObject();

    public ListSensorDataObject AllAccXSensorDataList { get; set; } = new ListSensorDataObject();
    public ListSensorDataObject AllAccYSensorDataList { get; set; } = new ListSensorDataObject();
    public ListSensorDataObject AllAccZSensorDataList { get; set; } = new ListSensorDataObject();

    public ListSensorDataObject AllGraviXSensorDataList { get; set; } = new ListSensorDataObject();
    public ListSensorDataObject AllGraviYSensorDataList { get; set; } = new ListSensorDataObject();
    public ListSensorDataObject AllGraviZSensorDataList { get; set; } = new ListSensorDataObject();
}
