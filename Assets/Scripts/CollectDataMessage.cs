using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CollectDataMessage : MonoBehaviour
{
    public GameObject generateMessage;
    public GameObject saveMessage;

    void Awake()
    {
        GetComponent<CollectDataController>().OnActionSucceed += ShowMessage;
    }

    void ShowMessage(MessageType mType, string text)
    {
        switch (mType)
        {
            case MessageType.GenerateMessage:
                generateMessage.GetComponent<TMP_Text>().text = text;
                break;
            case MessageType.SaveMessage:
                saveMessage.GetComponent<TMP_Text>().text = text;
                break;
            default:
                generateMessage.GetComponent<TMP_Text>().text = string.Empty;
                saveMessage.GetComponent<TMP_Text>().text = string.Empty;
                break;
        }
    }
}
