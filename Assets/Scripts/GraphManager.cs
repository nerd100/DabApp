using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GraphManager : MonoBehaviour
{
    public GameObject graphPanel;
    public GameObject GraphSection;

    public GameObject graphPointContainer;
    public GameObject graphPointPrefab;

    public List<GameObject> graphList;

    private float GraphBoxHeight = 0f;
    private float GraphBoxWidth = 0f;
    private float graphHeightScale = 0f;
    private float graphWidthScale = 0f;

    private float xOffset = 0f;

    public void DrawSingleSensorGraph(List<DataObject> dataList, int graphListNumber)
    {
        GraphBoxHeight = GraphSection.GetComponent<RectTransform>().rect.height;
        GraphBoxWidth = GraphSection.GetComponent<RectTransform>().rect.width;

        graphHeightScale = GraphBoxHeight / 2;
        graphWidthScale = GraphBoxWidth / 2;

        xOffset = GraphBoxWidth / 100;

        var graphRenderer = graphList[graphListNumber];

        var copyDataList = new List<DataObject>();
        copyDataList = dataList;
        var startPointNormalized = new List<DataObject>();
        var allPointsNormalized = new List<DataObject>();

        var startValue = dataList.Where(data => data.Id == 1).Select(data => data.Value).First();

        if (startValue > 0)
        {
            copyDataList.ForEach(data => startPointNormalized.Add(new DataObject(data.Id, data.Value - startValue)));
        }
        else
        {
            copyDataList.ForEach(data => startPointNormalized.Add(new DataObject(data.Id, data.Value + Mathf.Abs(startValue))));
        }

        var highestValue = startPointNormalized.Max(data => Mathf.Abs(data.Value));

        startPointNormalized.ForEach(data => allPointsNormalized.Add(new DataObject(data.Id, (data.Value / highestValue) * graphHeightScale)));

        var lineRenderer = graphRenderer.GetComponent<LineRenderer>();
        lineRenderer.positionCount = dataList.Count;
        for (int i = 0; i < dataList.Count; i++)
        {
            var xPos = -graphWidthScale + (i * (float)xOffset);
            lineRenderer.SetPosition(i, new Vector3(xPos, allPointsNormalized[i].Value, 0));
            var point = Instantiate(graphPointPrefab);
            point.transform.SetParent(graphPointContainer.transform);
            point.transform.localPosition = new Vector3(xPos, allPointsNormalized[i].Value, 0);

        }
    }

    public void DrawCombinedSensorGraph(List<SensorDataObject> allSensorDataList, int graphListNumber)
    {
        var combinedSensorData = ServiceLocator.Resolve<IReadWriteController>().CombineSensorData(allSensorDataList);
        DrawSingleSensorGraph(combinedSensorData, graphListNumber);
    }

    public void ClearPoints()
    {
        foreach (Transform child in graphPointContainer.transform)
        {
            Destroy(child.gameObject);
        }
    }
}