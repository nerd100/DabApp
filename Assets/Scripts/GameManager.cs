using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject mainPanel;
    public static Action OnStartEvaluation;

    void Awake()
    {
        ServiceLocator.Register<IPanelHandler>(GetComponent<IPanelHandler>());
        ServiceLocator.Register<IModalHandler>(GetComponent<IModalHandler>());
        ServiceLocator.Register<IActionHandler>(GetComponent<IActionHandler>());
        ServiceLocator.Register<ISensorService>(GetComponent<ISensorService>());
        ServiceLocator.Register<IReadWriteController>(new ReadWriteController());
        ServiceLocator.Register<ILeaderBaseController>(new LeaderBaseController());

        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int) GameStateEnum.StartScreen);
        ServiceLocator.Resolve<IModalHandler>().ChangeModal((int) ModalEnum.None);
        ServiceLocator.Resolve<IActionHandler>().ChangeAction((int) ActionTypeEnum.None);
    }

    public void StartEvaluation()
    {
        OnStartEvaluation?.Invoke();
    }

    public void StartDabGame()
    {
        gameObject.GetComponent<IModalHandler>().ChangeModal((int)ModalEnum.CountdownModal);
        gameObject.GetComponent<IActionHandler>().ChangeAction((int)ActionTypeEnum.RealDab);
        FindObjectOfType<Countdown>().StartCountdown();
    }

    public void StartMimicGame()
    {
        gameObject.GetComponent<IModalHandler>().ChangeModal((int)ModalEnum.CountdownMimicModal);
        if (gameObject.GetComponent<IActionHandler>().ActionTypeEnum == ActionTypeEnum.None)
        {
            gameObject.GetComponent<IActionHandler>().ChangeAction((int)ActionTypeEnum.MimicFirst);
        }
        FindObjectOfType<Countdown>().StartCountdown();
    }

    public void StartDataCollect()
    {
        gameObject.GetComponent<IModalHandler>().ChangeModal((int)ModalEnum.CountdownCollectDataModal);
        gameObject.GetComponent<IActionHandler>().ChangeAction((int)ActionTypeEnum.CollectDataDab);
        FindObjectOfType<Countdown>().StartCountdown();
    }
}
