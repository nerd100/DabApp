using UnityEngine;
using UnityEngine.UI;

public class PointsBar : MonoBehaviour
{
    [SerializeField]
    private GameObject slider;

    private void Awake()
    {
        GetComponent<IResultController>().OnAddPoints += UpdateSlider;
        GetComponent<IResultController>().OnResetPoints += Reset;
    }

    private void UpdateSlider(float currPoints)
    {
        slider.GetComponent<Slider>().value = currPoints;

        if (currPoints >= 200 && currPoints < 400)
        {
            slider.GetComponentsInChildren<Image>()[1].color = Color.yellow;
        }
        if (currPoints >= 400 && currPoints < 600)
        {
            slider.GetComponentsInChildren<Image>()[1].color = Color.cyan;
        }
        if (currPoints >= 600 && currPoints < 800)
        {
            slider.GetComponentsInChildren<Image>()[1].color = Color.green;
        }
        if (currPoints >= 800 && currPoints < 900)
        {
            slider.GetComponentsInChildren<Image>()[1].color = Color.magenta;
        }
        if (currPoints >= 900)
        {
            slider.GetComponentsInChildren<Image>()[1].color = new Color(243, 195, 40);
        }
    }

    private void Reset()
    {
        slider.GetComponent<Slider>().value = 0;
    }
}
