using System;

public interface IResultController
{
    event Action<float> OnAddPoints;
    event Action OnFinishAddPoints;
    event Action OnHandleScore;
    event Action OnResetPoints;
}
