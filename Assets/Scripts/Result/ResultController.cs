using UnityEngine;
using System;

public class ResultController : MonoBehaviour, IResultController
{
    private float resultPoints = 0.0f;
    private float currPoints = 0;

    private bool startEvalutaion = false;

    public event Action<float> OnAddPoints;
    public event Action OnFinishAddPoints;
    public event Action OnHandleScore;
    public event Action OnResetPoints;

    [SerializeField] private bool isEvaluationFinished = false;

    void Awake()
    {
        GameManager.OnStartEvaluation += StartEvaluation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (startEvalutaion && currPoints < resultPoints)
        {
            currPoints += 1;
            OnAddPoints?.Invoke(currPoints);
        }

        if(currPoints >= resultPoints && !isEvaluationFinished)
        {
            isEvaluationFinished = true;
            FindObjectOfType<AudioManager>().Stop("Slot");
            OnFinishAddPoints?.Invoke();
            if(ServiceLocator.Resolve<IActionHandler>().ActionTypeEnum == ActionTypeEnum.RealDab)
            {
                OnHandleScore?.Invoke();
            } 
        }
    }

    public void StartEvaluation()
    {
        string[] sounds = { "Gun1", "Gun2", "Gun3" };
        FindObjectOfType<AudioManager>().PlayRandom(sounds);

        resultPoints = DataHolder.FinalPoints;

        startEvalutaion = true;
        FindObjectOfType<AudioManager>().Play("Slot");
    }

    public void DrawGraph()
    {
        var graphManager = FindObjectOfType<GraphManager>();
        graphManager.DrawSingleSensorGraph(DataHolder.TargetNormalizedSensorDataList, 0);
        graphManager.DrawSingleSensorGraph(DataHolder.CurrentNormalizedSensorDataList, 1);
    }

    public void Retry()
    {
        //Reset all Points
        resultPoints = 0.0f;
        currPoints = 0;
        startEvalutaion = false;
        isEvaluationFinished = false;
        DataHolder.FinalPoints = 1000.0f;

        //Delete all PointPrefabs of Graph
        var graphManager = gameObject.transform.Find("StatistikModal/GraphPanel").GetComponent<GraphManager>();
        graphManager.GetComponent<GraphManager>().ClearPoints();

        //Reset Slider and PointCounter
        OnResetPoints();

        //Stop Audio
        FindObjectOfType<AudioManager>().Stop("Slot");

        var previouseGameState = ServiceLocator.Resolve<IPanelHandler>().PreviousState;
        if (previouseGameState == GameStateEnum.Dab)
        {
            ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.DabScreen);
        }
        else
        {
            ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.MimicScreen);
        }

        ServiceLocator.Resolve<IActionHandler>().ChangeAction((int)ActionTypeEnum.None);
    }
}
