using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PointsCounter : MonoBehaviour
{
    [SerializeField]
    private GameObject pointCounter;

    private void Awake()
    {
        GetComponent<IResultController>().OnAddPoints += UpdatePoints;
        GetComponent<IResultController>().OnResetPoints += Reset;
    }

    private void UpdatePoints(float currPoints)
    {
        pointCounter.GetComponent<TMP_Text>().text = currPoints.ToString();
    }

    private void Reset()
    {
        pointCounter.GetComponent<TMP_Text>().text = "0";
    }
}
