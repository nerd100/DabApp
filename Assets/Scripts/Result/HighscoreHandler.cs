using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighscoreHandler : MonoBehaviour
{
    [SerializeField] private GameObject scoreText;
    [SerializeField] private GameObject nameInputField;    

    void Awake()
    {
        GetComponent<IResultController>().OnHandleScore += InitScore;
    }

    private void InitScore()
    {
        var score = DataHolder.FinalPoints;

        var leaderController = ServiceLocator.Resolve<ILeaderBaseController>();
        leaderController.InitLeader();
        var isNewHightscore = leaderController.IsNewHighscore((int)score);
        if (isNewHightscore)
        {
            StartCoroutine(WaitForSeconds(1));
        }
    }

    private void HandleNewHighscore()
    {
        var score = DataHolder.FinalPoints;

        ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.NewHighscoreModal);
        scoreText.GetComponent<TMP_Text>().text = score.ToString();

        string[] sounds = { "Sheesh", "Scurr", "Yeah" };
        FindObjectOfType<AudioManager>().PlayRandom(sounds);
    }

    public void SetNewLeader()
    {
        var score = DataHolder.FinalPoints;
        var name = nameInputField.GetComponent<TMP_InputField>().text;
        nameInputField.GetComponent<TMP_InputField>().text = "";

        if(name == "")
        {
            return;
        }

        ServiceLocator.Resolve<ILeaderBaseController>().SetNewLeader(name, (int)score);
        ServiceLocator.Resolve<IModalHandler>().ChangeModal((int)ModalEnum.None);
    }

    private IEnumerator WaitForSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        HandleNewHighscore();
    }
}
