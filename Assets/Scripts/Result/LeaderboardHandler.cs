using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LeaderboardHandler : MonoBehaviour
{
    public GameObject leaderboardInner;

    public void CreateLeaderboard()
    {
        var top10Leader = ServiceLocator.Resolve<ILeaderBaseController>().Top10Leader;
        int children = leaderboardInner.transform.childCount;
        for (int i = 0; i < children; ++i) 
        {
            var child = leaderboardInner.transform.GetChild(i);
            var childTextComponents = child.GetComponentsInChildren<TMP_Text>();

            childTextComponents[0].text = top10Leader["leader" + (i + 1)].Name;
            childTextComponents[1].text = top10Leader["leader" + (i + 1)].Score.ToString();
        }
    }
}
