using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnableNavbar : MonoBehaviour
{
    public GameObject pointsButton;
    public GameObject statistikButton;
    public GameObject leaderbordButton;

    public void Awake()
    {
        GetComponent<IResultController>().OnFinishAddPoints += EnableNavbarButtons;
        GetComponent<IResultController>().OnResetPoints += DisableNavbarButtons;
    }

    private void EnableNavbarButtons()
    {
        pointsButton.GetComponent<Button>().interactable = true;
        statistikButton.GetComponent<Button>().interactable = true;
        leaderbordButton.GetComponent<Button>().interactable = true;
    }

    private void DisableNavbarButtons()
    {
        pointsButton.GetComponent<Button>().interactable = false;
        statistikButton.GetComponent<Button>().interactable = false;
        leaderbordButton.GetComponent<Button>().interactable = false;
    }
}
