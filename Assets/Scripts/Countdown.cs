using System.Collections;
using UnityEngine;
using TMPro;

public class Countdown : MonoBehaviour
{
    public int counter;
    bool updateCounter;
    
    private bool areYouReady = false;

    public void StartCountdown()
    {
        InitCountdown();
    }

    void Update()
    {
        if(areYouReady)
        {  
            if (updateCounter)
            {
                if(counter > 0)
                {
                    gameObject.GetComponentInChildren<TMP_Text>().fontSize = 250;
                    gameObject.GetComponentInChildren<TMP_Text>().text = counter.ToString();
                    PlaySound(counter.ToString());
                    StartCoroutine(WaitForSeconds(1));
                }
                if (counter == 0)
                {
                    updateCounter = false;
                    gameObject.GetComponentInChildren<TMP_Text>().fontSize = 100;
                    gameObject.GetComponentInChildren<TMP_Text>().text = "Dab";
                    PlaySound("Dab");

                    var currentGameState = ServiceLocator.Resolve<IPanelHandler>().CurrentState;
                    if(currentGameState == GameStateEnum.DabScreen)
                    {
                        ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.Dab);
                    }
                    else if(currentGameState == GameStateEnum.MimicScreen)
                    {
                        ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.Mimic);
                    }
                    else
                    {
                        ServiceLocator.Resolve<IPanelHandler>().ChangePanel((int)GameStateEnum.CollectDataDab);
                    }

                    ServiceLocator.Resolve<ISensorService>().InitAndActivateSensors();
                }
            }
        }
    }

    private void InitCountdown()
    {
        gameObject.GetComponentInChildren<TMP_Text>().text = "";

        counter = 3;
        updateCounter = false;
        areYouReady = false;

        if (!areYouReady)
        {
            var clipLentgh = GetSoundLentgh("Ready");
            StartCoroutine(WaitForReady(clipLentgh));
        }
    }

    private IEnumerator WaitForSeconds(float seconds)
    {
        updateCounter = false;
        yield return new WaitForSeconds(seconds);
        counter--;
        updateCounter = true;
    }

    private IEnumerator WaitForReady(float seconds)
    {
        PlaySound("Ready");
        yield return new WaitForSeconds(seconds);
        areYouReady = true;
        updateCounter = true;
    }

    private void PlaySound(string name)
    {
        FindObjectOfType<AudioManager>().Play(name);
    }

    private float GetSoundLentgh(string name)
    {
        return FindObjectOfType<AudioManager>().Length(name);
    }
}
