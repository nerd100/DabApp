public enum ModalEnum
{
    None = 0,
    VerdabbtModal = 1,
    CountdownModal = 2,
    StatisikModal = 3,
    LeaderboardModal = 4,
    CollectDataGraphModal = 5,
    CountdownMimicModal = 6,
    CountdownCollectDataModal = 7,
    NewHighscoreModal = 8,
}
