public enum MessageType
{
    None = 0,
    GenerateMessage = 1,
    SaveMessage = 2
}
