public enum GameStateEnum
{
   StartScreen = 0,
   DabScreen = 1,
   CollectScreen = 2,
   ResultScreen = 3,
   Dab = 4,
   MimicScreen = 5,
   Mimic = 6,
   CollectDataDab = 7,
}
