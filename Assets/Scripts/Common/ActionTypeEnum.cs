public enum ActionTypeEnum
{
    None = 0,
    CollectDataDab = 1,
    RealDab = 2,
    MimicFirst = 3,
    MimicSecond = 4,
}
