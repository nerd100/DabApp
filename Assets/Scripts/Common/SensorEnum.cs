public enum SensorEnum
{
    GyroX = 1,
    GyroY = 2,
    GyroZ = 3,
    AccX = 4,
    AccY = 5,
    AccZ = 6,
    GraviX = 7,
    GraviY = 8,
    GraviZ = 9
}
