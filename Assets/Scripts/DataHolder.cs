using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHolder : MonoBehaviour
{
    public static List<DataObject> TargetNormalizedSensorDataList { get; set; }
    public static List<DataObject> CurrentNormalizedSensorDataList { get; set; }

    public static float FinalPoints { get; set; } = 1000.0f;

    public static List<SensorDataObject> currentRawData { get; set; }

    public static List<SensorDataObject> mimicFirstRawData { get; set; }
    public static List<SensorDataObject> mimicSecondRawData { get; set; }
}
